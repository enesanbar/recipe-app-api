terraform {
  required_providers {
    aws = {
      version = "~> 4.8.0"
    }
  }

  required_version = ">= 1.1.9"

  backend "s3" {
    region  = "eu-central-1"
    bucket  = "terraform-remote-state-9424175f6"
    key     = "recipe-app.tfstate"
    encrypt = true
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}