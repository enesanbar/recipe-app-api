resource "aws_vpc" "main" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    {
      Name = "${local.prefix}-vpc"
    }
  )
}

resource "aws_internet_gateway" "main-igw" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    {
      Name = "${local.prefix}-main-igw"
    }
  )
}


#####################################################
# Public Subnets - Inbound/Outbound Internet Access #
#####################################################
resource "aws_subnet" "main-public-a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-a"
    })
  )
}

resource "aws_route_table" "main-public-a-route-table" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-igw.id
  }

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-a"
    })
  )
}

resource "aws_route_table_association" "main-public-a-route-table-association" {
  subnet_id      = aws_subnet.main-public-a.id
  route_table_id = aws_route_table.main-public-a-route-table.id
}

resource "aws_eip" "main-public-a" {
  vpc = true

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-a"
    })
  )
}

resource "aws_nat_gateway" "main-public-a-ng" {
  allocation_id = aws_eip.main-public-a.id
  subnet_id     = aws_subnet.main-public-a.id

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-a"
    })
  )
}

resource "aws_subnet" "main-public-b" {
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-b"
    })
  )
}

resource "aws_route_table" "main-public-b" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-igw.id
  }

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-b"
    })
  )
}

resource "aws_route_table_association" "main-public-b" {
  subnet_id      = aws_subnet.main-public-b.id
  route_table_id = aws_route_table.main-public-b.id
}

resource "aws_eip" "main-public-b" {
  vpc = true

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-b"
    })
  )
}

resource "aws_nat_gateway" "main-public-b-ng" {
  allocation_id = aws_eip.main-public-b.id
  subnet_id     = aws_subnet.main-public-b.id

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-b"
    })
  )
}

###################################################
# Private Subnets - Outbound internet access only #
###################################################
resource "aws_subnet" "main-private-a" {
  cidr_block        = "10.1.10.0/24"
  vpc_id            = aws_vpc.main.id
  availability_zone = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-a"
    })
  )
}

resource "aws_route_table" "main-private-a" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main-public-a-ng.id
  }

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-a"
    })
  )
}

resource "aws_route_table_association" "main-private-a" {
  subnet_id      = aws_subnet.main-private-a.id
  route_table_id = aws_route_table.main-private-a.id
}

resource "aws_subnet" "main-private-b" {
  cidr_block        = "10.1.11.0/24"
  vpc_id            = aws_vpc.main.id
  availability_zone = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-b"
    })
  )
}

resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main-public-b-ng.id
  }

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-b"
    })
  )
}

resource "aws_route_table_association" "main-private-b" {
  subnet_id      = aws_subnet.main-private-b.id
  route_table_id = aws_route_table.private_b.id
}
