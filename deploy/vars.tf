variable "AWS_REGION" {
  default     = "eu-central-1"
  description = "AWS Region"
}

variable "AWS_PROFILE" {
  default = "default"
}

variable "prefix" {
  default = "raa"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "example@example.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-api-app-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "876669042460.dkr.ecr.eu-central-1.amazonaws.com/recipe-api-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for Proxy"
  default     = "876669042460.dkr.ecr.eu-central-1.amazonaws.com/recipe-api-app-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
